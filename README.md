# Retro Arcade

## Tabla de contenidos / Table of Contents
1. [ES - Que soy?](#que-soy)
2. [ES - Que puedo hacer?](#que-puedo-hacer)
    + [ES - Iniciar sesion o registrarte](#iniciar-sesion-o-registrarte)
    + [ES - Inicio](#inicio)
    + [ES - Juegos](#juegos)
    + [ES - Musica](#musica)
    + [ES - Jugadores](#jugadores)
    + [ES - Perfil](#perfil)
    + [ES - Cerrar sesion](#cerrar-sesion)
3. [ES - Sobre mi](#sobre-mi)
4. [EN - That I am?](#that-i-am)
5. [EN - What can I do?](#what-can-i-do)
    + [EN - Login or register](#login-or-register)
    + [EN - Home](#home-inicio)
    + [EN - Games](#games-juegos)
    + [EN - Music](#music-musica)
    + [EN - Players](#players-jugadores)
    + [EN - Profile](#profile-profile)
    + [EN - Sign off](#sign-off-cerrar-sesion)
6. [EN - About me](#about-me)


---

## Que soy? 

Retro Arcade es un pequeño projecto de JavaFx que se creo como tarea en el CFGS Desarrollo de Aplicaciones Multimedia (DAM). Originalmente estaba pensado en unirse a otro proyecto de java que podia conectarse a una base de datos local, pudiendo elegir entre MySQL y MongoDB, esta recogia los datos y la aplicacion de escritorio los imprimia en una interfaz amigable.

Soy aplicacion muy basica y en esta version no estoy conectada con el otro proyecto anteriormente mencionado, asi que utilizo como remplazo unos archivos .txt separados por ;.

## Que puedo hacer?

### Iniciar sesion o registrarte
Tenemos una pantalla para inicializar sesion con su correspondiente alternativa de registrar (si quieres probar te dejo el usuario: **demo**, con la contraseña: **1234**).

### Inicio
Una vez estamos dentro tenemos la tipica pantalla de inicio donde nos explica que es MySQL MongoDB y podriamos seleccionar contra cual base de datos queremos trabajar, pero como ya hemos dicho en esta version no se ha puesto asi que estara solo disponible la version de texto.

### Juegos
Esta es una pantalla de la base de datos, en ella se nos mostrara los juegos que existen y las 4 acciones posibles:

- Abir los detalles de un juego existente
- Insertar un nuevo juego
- Editar un juego existente
- Eliminar un juego existente

### Musica
Esta es una pantalla de la base de datos, en ella se nos mostrara las canciones que existen y las 4 acciones posibles:

- Abir los detalles de una cancion existente
- Insertar una nueva cancion
- Editar una cancion existente
- Eliminar una cancion existente

### Jugadores
Esta es una pantalla de la base de datos, en ella se nos mostrara los usuarios que existen y las 3 acciones posibles:

- Abir los detalles de un usuario existente
- Editar un usuario existente
- Eliminar un usuario existente

### Perfil
Esta es una pantalla en la que se muestra el usuario que se ha iniciado sesion y se puede editar el nombre de usuario o el del jugador.

### Cerrar sesion
Obviamente este boton nos cerrara la sesion e iremos de nuevo a la pantalla de inicio de sesion.


## Sobre mi

Estoy insipirada en los movimientos vaperwave, que para quien no sepa lo que es, es un genero que inspira tanto musica electronica como estilo artistico (aun que para ser francos, esto surgio como meme a mediados de la decada 2010).

- Musicalmente se dedica a mezclar generos como el *indie*, *seapunk* o el *chillwave* con estilos de otras epocas, sobretodo de finales de los años 70 y los años 80, como el *funk*, el *New Age* o el *city pop*.
- Artisiticamente mezcla elementos como *softwware* y diseño web de finales de los años 90, las imagenes pixeladas, esculturas griegas clasicas, pinturas renacentistas, anime, la lengua japonesa, la publicidad de epocas pasadas, el renderizado 3D, colores vivos y tematicas de ciencia ficcion y *ciberpunk*. Aqui te dejo un ejemplo de ello:

[![Vaporwave ejemplo](https://i2.wp.com/www.shutterstock.com/es/blog/wp-content/uploads/sites/18/2022/04/vaporwave9.jpg)](https://www.google.com/search?q=vaporwave)


---

## That I am?

Retro Arcade is a small JavaFX project that was created as a homework assignment in CFGS Multimedia Application Development (DAM in Spain). Originally, it was intended to join another Java project that could connect to a local database, allowing you to choose between MySQL and MongoDB. It collected the data, and the desktop application printed it with a friendly interface.

I am a very basic application, and in this version I am not connected with the other project mentioned above, so I use as a replacement some .txt files separated by ;.

## What can I do?

### Login or register
We have a screen to login and his corresponding alternative to register (if you want to try, I leave you the user: **demo**, with the password: **1234**).

### Home (Inicio)
Once we are inside, we have the typical home screen where it explains what MySQL and MongoDB are, and we could select which database we want to work against, but as we have already said, in this version it has not been set, so only the text version will be available.

### Games (Juegos)
This is a screen from the database, it will show us the games that exist and the 4 possible actions:

- Open the details of an existing game.
- Insert a new game.
- Edit an existing game.
- Delete an existing game.

### Music (Musica)
This is a screen from the database, it will show us the songs that exist and the 4 possible actions:

- Open the details of an existing song.
- Insert a new song.
- Edit an existing song.
- Delete an existing song.

### Players (Jugadores)
This is a screen from the database, it will show us the users that exist and the 3 possible actions:

- Open the details of an existing user.
- Edit an existing user.
- Delete an existing user.

### Profile (Profile)
This is a screen that shows the user logged in and you can edit the username or the player name.

### Sign off (Cerrar sesion)
Obviously, this button will close the session and we will go back to the login screen.


## About me

I am inspired by the vaperwave movements, which for those who don't know what it is, is a genre that inspires both electronic music and artistic style (to be clear, this emerged as a meme in the mid-2010s).

- Musically, it is dedicated to mixing genres such as *indie*, *seapunk* or *chillwave* with styles from other eras, especially from the late 70s and 80s, such as *funk*, *New Age* or the *city pop*.
- Artistically, it mixes elements such as *software* and web design from the late 1990s, pixelated images, classical Greek sculptures, Renaissance paintings, anime, the Japanese language, advertising from bygone eras, 3D rendering, vivid colors and themes like science fiction and *cyberpunk*. Here is an example of it:

[![Vaporwave example](https://i2.wp.com/www.shutterstock.com/es/blog/wp-content/uploads/sites/18/2022/04/vaporwave9.jpg)](https://www.google.com/search?q=vaporwave)