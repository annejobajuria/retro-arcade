module com.anne.retoArcade {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.anne.retoArcade to javafx.fxml;
    exports com.anne.retoArcade;
    exports com.anne.retoArcade.model;
    opens com.anne.retoArcade.model to javafx.fxml;
    exports com.anne.retoArcade.controller.actions;
    opens com.anne.retoArcade.controller.actions to javafx.fxml;
    exports com.anne.retoArcade.controller;
    opens com.anne.retoArcade.controller to javafx.fxml;
}