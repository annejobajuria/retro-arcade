package com.anne.retoArcade;

import com.anne.retoArcade.model.*;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;

import java.io.IOException;

public class ChangeSceneMethods {

    @FXML public void ToLogin() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("login.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        Application80s.stage_app.setScene(scene);
        Application80s.stage_app.show();
    }

    @FXML public void ToSignup() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("signup.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        Application80s.stage_app.setScene(scene);
        Application80s.stage_app.show();
    }

    // ----------------------------------------------------------------------------------

    @FXML public void ToInicio() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("inicio.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        Application80s.stage_app.setScene(scene);
        Application80s.stage_app.show();
    }

    @FXML public void ToGame() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("juegos.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        Application80s.stage_app.setScene(scene);
        Application80s.stage_app.show();
    }

    @FXML public void ToMusic() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("musica.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        Application80s.stage_app.setScene(scene);
        Application80s.stage_app.show();
    }

    @FXML public void ToJugadores() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("jugadores.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        Application80s.stage_app.setScene(scene);
        Application80s.stage_app.show();
    }

    @FXML public void ToPerfil() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("perfil.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        Application80s.stage_app.setScene(scene);
        Application80s.stage_app.show();
    }

    // ----------------------------------------------------------------------------------

    @FXML public void ToDetailJuego(Juego juego) throws IOException {
        if (juego==null) return;
        JuegoHelper helper = JuegoHelper.getInstance();
        helper.setJuego(juego);

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("actions/juegosDetail.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        Application80s.stage_app.setScene(scene);
        Application80s.stage_app.show();
    }

    @FXML public void ToInsertJuego() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("actions/juegosInsert.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        Application80s.stage_app.setScene(scene);
        Application80s.stage_app.show();
    }

    @FXML public void ToUpdateJuego(Juego juego) throws IOException {
        if (juego==null) return;
        JuegoHelper helper = JuegoHelper.getInstance();
        helper.setJuego(juego);

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("actions/juegosUpdate.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        Application80s.stage_app.setScene(scene);
        Application80s.stage_app.show();
    }

    // ----------------------------------------------------------------------------------

    @FXML public void ToDetailMusica(Musica musica) throws IOException {
        if (musica==null) return;
        MusicaHelper helper = MusicaHelper.getInstance();
        helper.setMusica(musica);

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("actions/musicaDetail.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        Application80s.stage_app.setScene(scene);
        Application80s.stage_app.show();
    }

    @FXML public void ToInsertMusica() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("actions/musicaInsert.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        Application80s.stage_app.setScene(scene);
        Application80s.stage_app.show();
    }

    @FXML public void ToUpdateMusica(Musica musica) throws IOException {
        if (musica==null) return;
        MusicaHelper helper = MusicaHelper.getInstance();
        helper.setMusica(musica);

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("actions/musicaUpdate.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        Application80s.stage_app.setScene(scene);
        Application80s.stage_app.show();
    }

    // ----------------------------------------------------------------------------------

    @FXML public void ToDetailJugador(Player player) throws IOException {
        if (player==null) return;
        PlayerHelper helper = PlayerHelper.getInstance();
        helper.setPlayer(player);

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("actions/jugadoresDetail.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        Application80s.stage_app.setScene(scene);
        Application80s.stage_app.show();
    }

    @FXML public void ToUpdateJugador(Player user) throws IOException {
        if (user==null) return;
        PlayerHelper helper = PlayerHelper.getInstance();
        helper.setPlayer(user);

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("actions/jugadoresUpdate.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        Application80s.stage_app.setScene(scene);
        Application80s.stage_app.show();
    }

    // ----------------------------------------------------------------------------------

    @FXML public void ToUpdatePerfil(User user) throws IOException {
        if (user==null) return;
        UserHelper helper = UserHelper.getInstance();
        helper.setUser(user);

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("actions/perfilUpdate.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        Application80s.stage_app.setScene(scene);
        Application80s.stage_app.show();
    }


}
