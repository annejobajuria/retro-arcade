package com.anne.retoArcade.model;

public class JuegoHelper {
    private Juego juego;
    private final static JuegoHelper INSTANCE = new JuegoHelper();

    public JuegoHelper() {
    }

    public static JuegoHelper getInstance(){
        return INSTANCE;
    }

    public Juego getJuego() {
        return juego;
    }

    public void setJuego(Juego juego) {
        this.juego = juego;
    }
}