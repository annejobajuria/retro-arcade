package com.anne.retoArcade.model;

public class MusicaHelper {
    private Musica musica;
    private final static MusicaHelper INSTANCE = new MusicaHelper();

    public MusicaHelper() {
    }

    public static MusicaHelper getInstance(){
        return INSTANCE;
    }

    public Musica getMusica() {
        return musica;
    }

    public void setMusica(Musica musica) {
        this.musica = musica;
    }
}
