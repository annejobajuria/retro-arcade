package com.anne.retoArcade.model;

public class PlayerHelper {
    private Player player;
    private final static PlayerHelper INSTANCE = new PlayerHelper();

    public PlayerHelper() {
    }

    public static PlayerHelper getInstance(){
        return INSTANCE;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
