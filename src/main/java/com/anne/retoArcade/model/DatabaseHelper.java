package com.anne.retoArcade.model;

import javafx.scene.control.RadioButton;

public class DatabaseHelper {
    private RadioButton selected;
    private final static DatabaseHelper INSTANCE = new DatabaseHelper();

    public DatabaseHelper() {
    }

    public static DatabaseHelper getInstance(){
        return INSTANCE;
    }

    public RadioButton getSelected() {
        return selected;
    }

    public void setSelected(RadioButton selected) {
        this.selected = selected;
    }
}
