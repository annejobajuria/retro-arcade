package com.anne.retoArcade.model;

import javafx.beans.property.SimpleStringProperty;

public class Musica {
    private final SimpleStringProperty id;
    private final SimpleStringProperty title;
    private final SimpleStringProperty artist;
    private final SimpleStringProperty disk;
    private final SimpleStringProperty year;

    public Musica(String id, String title, String artist, String disk, String year) {
        this.id = new SimpleStringProperty(id);
        this.title = new SimpleStringProperty(title);
        this.artist = new SimpleStringProperty(artist);
        this.disk = new SimpleStringProperty(disk);
        this.year = new SimpleStringProperty(year);
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getTitle() {
        return title.get();
    }

    public SimpleStringProperty titleProperty() {
        return title;
    }

    public void setTitle(String title) {
        this.title.set(title);
    }

    public String getArtist() {
        return artist.get();
    }

    public SimpleStringProperty artistProperty() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist.set(artist);
    }

    public String getDisk() {
        return disk.get();
    }

    public SimpleStringProperty diskProperty() {
        return disk;
    }

    public void setDisk(String disk) {
        this.disk.set(disk);
    }

    public String getYear() {
        return year.get();
    }

    public SimpleStringProperty yearProperty() {
        return year;
    }

    public void setYear(String year) {
        this.year.set(year);
    }
}