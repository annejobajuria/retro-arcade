package com.anne.retoArcade.model;

import javafx.beans.property.SimpleStringProperty;

public class Juego {
    private final SimpleStringProperty id;
    private final SimpleStringProperty title;
    private final SimpleStringProperty year;
    private final SimpleStringProperty desc;

    public Juego(String id, String title, String year, String desc) {
        this.id = new SimpleStringProperty(id);
        this.title = new SimpleStringProperty(title);
        this.year = new SimpleStringProperty(year);
        this.desc = new SimpleStringProperty(desc);
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getTitle() {
        return title.get();
    }

    public SimpleStringProperty titleProperty() {
        return title;
    }

    public void setTitle(String title) {
        this.title.set(title);
    }

    public String getYear() {
        return year.get();
    }

    public SimpleStringProperty yearProperty() {
        return year;
    }

    public void setYear(String year) {
        this.year.set(year);
    }

    public String getDesc() {
        return desc.get();
    }

    public SimpleStringProperty descProperty() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc.set(desc);
    }
}