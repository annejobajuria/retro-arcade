package com.anne.retoArcade.model;

public class UserHelper {
    private User user;
    private final static UserHelper INSTANCE = new UserHelper();

    public UserHelper() {
    }

    public static UserHelper getInstance(){
        return INSTANCE;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
