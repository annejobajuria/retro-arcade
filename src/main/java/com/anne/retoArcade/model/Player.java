package com.anne.retoArcade.model;

import javafx.beans.property.SimpleStringProperty;

public class Player {
    private final SimpleStringProperty username;
    private final SimpleStringProperty password;
    private final SimpleStringProperty playername;
    private final SimpleStringProperty game1;
    private final SimpleStringProperty game2;
    private final SimpleStringProperty game3;

    public Player(String username, String password, String playername, String game1, String game2, String game3) {
        this.username = new SimpleStringProperty(username);
        this.password = new SimpleStringProperty(password);
        this.playername = new SimpleStringProperty(playername);
        this.game1 = new SimpleStringProperty(game1);
        this.game2 = new SimpleStringProperty(game2);
        this.game3 = new SimpleStringProperty(game3);
    }

    public String getUsername() {
        return username.get();
    }

    public SimpleStringProperty usernameProperty() {
        return username;
    }

    public void setUsername(String username) {
        this.username.set(username);
    }

    public String getPassword() {
        return password.get();
    }

    public SimpleStringProperty passwordProperty() {
        return password;
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

    public String getPlayername() {
        return playername.get();
    }

    public SimpleStringProperty playernameProperty() {
        return playername;
    }

    public void setPlayername(String playername) {
        this.playername.set(playername);
    }

    public String getGame1() {
        return game1.get();
    }

    public SimpleStringProperty game1Property() {
        return game1;
    }

    public void setGame1(String game1) {
        this.game1.set(game1);
    }

    public String getGame2() {
        return game2.get();
    }

    public SimpleStringProperty game2Property() {
        return game2;
    }

    public void setGame2(String game2) {
        this.game2.set(game2);
    }

    public String getGame3() {
        return game3.get();
    }

    public SimpleStringProperty game3Property() {
        return game3;
    }

    public void setGame3(String game3) {
        this.game3.set(game3);
    }
}
