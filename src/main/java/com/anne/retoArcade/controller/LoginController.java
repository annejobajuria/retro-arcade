package com.anne.retoArcade.controller;

import com.anne.retoArcade.ChangeSceneMethods;
import com.anne.retoArcade.model.User;
import com.anne.retoArcade.model.UserHelper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

public class LoginController implements Initializable {
    ChangeSceneMethods change = new ChangeSceneMethods();
    private List<User> users;
    @FXML private Button loginBtn;
    @FXML private TextField usernameLogin;
    @FXML private PasswordField passLogin;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        users = fillList();
        loginBtn.disableProperty().bind(
                usernameLogin.textProperty().isEmpty().or(
                passLogin.textProperty().isEmpty()
                ));
    }

    public List<User> fillList() {
        List<String> list = new ArrayList<>();
        List<User> listUsers = new ArrayList<>();

        File file = new File("./src/main/java/com/anne/retoArcade/Data/users.txt");
        if (file.exists()) {
            try {
                list = Files.readAllLines(file.toPath(), Charset.defaultCharset());
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (list.isEmpty())
                return null;
        }
        for (String line : list) {
            if (!line.equals("")) {
                String[] res = line.split(";");
                listUsers.add(new User(res[0], res[1], res[2], res[3], res[4], res[5]));
            }
        }
        return listUsers;
    }

    @FXML public void verify() throws IOException {
        User user = null;
        for (User u : users) {
            if (u.getUsername().toLowerCase(Locale.ROOT).equals(usernameLogin.getText().toLowerCase(Locale.ROOT)) &&
                    u.getPassword().toLowerCase(Locale.ROOT).equals(passLogin.getText().toLowerCase(Locale.ROOT)) ) {
                user = new User(u.getUsername(), u.getPassword(), u.getPlayername(), u.getGame1(), u.getGame2(), u.getGame3());
            }
        }

        if (user != null) {
            UserHelper holder = UserHelper.getInstance();
            holder.setUser(user);
            change.ToInicio();
        } else {
            System.out.println("No estas registrado o te has equivocado");
        }
    }

    // ----------------------------------------------------------------------------------

    @FXML public void changeToSignup() throws IOException {
        change.ToSignup();
    }
}
