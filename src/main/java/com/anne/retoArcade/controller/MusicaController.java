package com.anne.retoArcade.controller;

import com.anne.retoArcade.ChangeSceneMethods;
import com.anne.retoArcade.model.DatabaseHelper;
import com.anne.retoArcade.model.Musica;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class MusicaController implements Initializable {
    ChangeSceneMethods change = new ChangeSceneMethods();
    @FXML private TableView<Musica> table;
    @FXML private TableColumn<Musica, String> id;
    @FXML private TableColumn<Musica, String> title;
    @FXML private TableColumn<Musica, String> artist;
    @FXML private TableColumn<Musica, String> disk;
    @FXML private TableColumn<Musica, String> year;
    RadioButton database = DatabaseHelper.getInstance().getSelected();
    @FXML private Text databaseTxt;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        id.setCellValueFactory(new PropertyValueFactory<>("id"));
        title.setCellValueFactory(new PropertyValueFactory<>("title"));
        artist.setCellValueFactory(new PropertyValueFactory<>("artist"));
        title.setCellValueFactory(new PropertyValueFactory<>("title"));
        disk.setCellValueFactory(new PropertyValueFactory<>("disk"));
        year.setCellValueFactory(new PropertyValueFactory<>("year"));
        ObservableList<Musica> musicas = FXCollections.observableArrayList(fillList());
        table.setItems(musicas);
        databaseTxt.setText(database.getText());
    }

    public List<Musica> fillList() {
        List<String> list = new ArrayList<>();
        List<Musica> listMusica = new ArrayList<>();

        File file = new File("./src/main/java/com/anne/retoArcade/Data/musica.txt");
        if (file.exists()) {
            try {
                list = Files.readAllLines(file.toPath(), Charset.defaultCharset());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        for (String line : list) {
            if (!line.equals("")) {
                String[] res = line.split(";");
                listMusica.add(new Musica(res[0], res[1], res[2], res[3], res[4]));
            }
        }
        return listMusica;
    }

    @FXML public void viewDetails() throws IOException {
        Musica musica = table.getSelectionModel().getSelectedItem();
        change.ToDetailMusica(musica);
    }

    @FXML public void insert() throws IOException {
        change.ToInsertMusica();
    }

    @FXML public void update() throws IOException {
        Musica musica = table.getSelectionModel().getSelectedItem();
        change.ToUpdateMusica(musica);
    }

    @FXML public void delete() throws IOException {
        Musica musica = table.getSelectionModel().getSelectedItem();
        if (musica==null) return;
        String oldline = musica.getId()+";"+musica.getTitle()+";"+musica.getArtist()+";"+musica.getDisk()+";"+musica.getYear();
        File file = new File("./src/main/java/com/anne/retoArcade/Data/musica.txt");
        List<String> out = Files.lines(file.toPath())
                .filter(line -> !line.contains(oldline))
                .collect(Collectors.toList());
        Files.write(file.toPath(), out, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
        change.ToMusic();
    }

    // ----------------------------------------------------------------------------------

    @FXML public void changeToInicio() throws IOException {
        change.ToInicio();
    }

    @FXML public void changeToGame() throws IOException {
        change.ToGame();
    }

    @FXML public void changeToMusic() throws IOException {
        change.ToMusic();
    }

    @FXML public void changeToJugadores() throws IOException {
        change.ToJugadores();
    }

    @FXML public void changeToPerfil() throws IOException {
        change.ToPerfil();
    }

    @FXML public void goToLogin() throws IOException {
        change.ToLogin();
    }
}