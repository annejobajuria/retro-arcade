package com.anne.retoArcade.controller;

import com.anne.retoArcade.ChangeSceneMethods;
import com.anne.retoArcade.model.DatabaseHelper;
import com.anne.retoArcade.model.Juego;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class JuegosController implements Initializable {
    ChangeSceneMethods change = new ChangeSceneMethods();
    @FXML private TableView<Juego> table;
    @FXML private TableColumn<Juego, String> id;
    @FXML private TableColumn<Juego, String> title;
    @FXML private TableColumn<Juego, String> year;
    @FXML private TableColumn<Juego, String> description;
    RadioButton database = DatabaseHelper.getInstance().getSelected();
    @FXML private Text databaseTxt;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        id.setCellValueFactory(new PropertyValueFactory<>("id"));
        title.setCellValueFactory(new PropertyValueFactory<>("title"));
        year.setCellValueFactory(new PropertyValueFactory<>("year"));
        description.setCellValueFactory(new PropertyValueFactory<>("desc"));
        ObservableList<Juego> juegos = FXCollections.observableArrayList(fillList());
        table.setItems(juegos);
        databaseTxt.setText(database.getText());
    }

    public List<Juego> fillList() {
        List<String> list = new ArrayList<>();
        List<Juego> listGames = new ArrayList<>();

        File file = new File("./src/main/java/com/anne/retoArcade/Data/juegos.txt");
        if (file.exists()) {
            try {
                list = Files.readAllLines(file.toPath(), Charset.defaultCharset());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        for (String line : list) {
            if (!line.equals("")) {
                String[] res = line.split(";");
                listGames.add(new Juego(res[0], res[1], res[2], res[3]));
            }
        }
        return listGames;
    }

    @FXML public void viewDetails() throws IOException {
        Juego juego = table.getSelectionModel().getSelectedItem();
        change.ToDetailJuego(juego);
    }

    @FXML public void insert() throws IOException {
        change.ToInsertJuego();
    }

    @FXML public void update() throws IOException {
        Juego juego = table.getSelectionModel().getSelectedItem();
        change.ToUpdateJuego(juego);
    }

    @FXML public void delete() throws IOException {
        Juego juego = table.getSelectionModel().getSelectedItem();
        if (juego==null) return;
        String oldline = juego.getId()+";"+juego.getTitle()+";"+juego.getYear()+";"+juego.getDesc();
        File file = new File("./src/main/java/com/anne/retoArcade/Data/juegos.txt");
        List<String> out = Files.lines(file.toPath())
                .filter(line -> !line.contains(oldline))
                .collect(Collectors.toList());
        Files.write(file.toPath(), out, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
        change.ToGame();
    }

    // ----------------------------------------------------------------------------------

    @FXML public void changeToInicio() throws IOException {
        change.ToInicio();
    }

    @FXML public void changeToGame() throws IOException {
        change.ToGame();
    }

    @FXML public void changeToMusic() throws IOException {
        change.ToMusic();
    }

    @FXML public void changeToJugadores() throws IOException {
        change.ToJugadores();
    }

    @FXML public void changeToPerfil() throws IOException {
        change.ToPerfil();
    }

    @FXML public void goToLogin() throws IOException {
        change.ToLogin();
    }
}