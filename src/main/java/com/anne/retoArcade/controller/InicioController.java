package com.anne.retoArcade.controller;

import com.anne.retoArcade.ChangeSceneMethods;
import com.anne.retoArcade.model.DatabaseHelper;
import com.anne.retoArcade.model.User;
import com.anne.retoArcade.model.UserHelper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.text.Text;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class InicioController implements Initializable {
    ChangeSceneMethods change = new ChangeSceneMethods();
    User user = UserHelper.getInstance().getUser();
    DatabaseHelper holder = DatabaseHelper.getInstance();
    RadioButton database = DatabaseHelper.getInstance().getSelected();
    @FXML private Text username;
    @FXML private Text playername;
    @FXML private ToggleGroup databaseTG;
    @FXML private RadioButton gitLabRB, mysqlRB, mongodbRB;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        username.setText(user.getUsername().substring(0,1).toUpperCase() + user.getUsername().substring(1));
        playername.setText(user.getPlayername().toUpperCase());

        if (database == null) {
            gitLabRB.setSelected(true);
            RadioButton databaseSelected = (RadioButton) databaseTG.getSelectedToggle();
            holder.setSelected(databaseSelected);
        } else {
            switch (database.getId()) {
                case "gitLabRB":
                    gitLabRB.setSelected(true);
                    break;
                case "mysqlRB":
                    mysqlRB.setSelected(true);
                    break;
                case "mongodbRB":
                    mongodbRB.setSelected(true);
                    break;
                default:
                    break;
            }
            database.setSelected(true);
        }
    }

    @FXML public void verify() {
        database = DatabaseHelper.getInstance().getSelected();
        RadioButton databaseSelected = (RadioButton) databaseTG.getSelectedToggle();
        holder.setSelected(databaseSelected);
    }

    // ----------------------------------------------------------------------------------

    @FXML public void changeToInicio() throws IOException {
        change.ToInicio();
    }

    @FXML public void changeToGame() throws IOException {
        change.ToGame();
    }

    @FXML public void changeToMusic() throws IOException {
        change.ToMusic();
    }

    @FXML public void changeToJugadores() throws IOException {
        change.ToJugadores();
    }

    @FXML public void changeToPerfil() throws IOException {
        change.ToPerfil();
    }

    @FXML public void goToLogin() throws IOException {
        change.ToLogin();
    }

    // ----------------------------------------------------------------------------------

    @FXML public void wikiSql() {
        try {
            Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + "http://es.wikipedia.org/wiki/MySQL");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML public void wikiMongo() {
        try {
            Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + "http://es.wikipedia.org/wiki/MongoDB");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}