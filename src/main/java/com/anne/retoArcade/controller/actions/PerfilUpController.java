package com.anne.retoArcade.controller.actions;

import com.anne.retoArcade.ChangeSceneMethods;
import com.anne.retoArcade.model.DatabaseHelper;
import com.anne.retoArcade.model.User;
import com.anne.retoArcade.model.UserHelper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

public class PerfilUpController implements Initializable {
    ChangeSceneMethods change = new ChangeSceneMethods();
    User user = UserHelper.getInstance().getUser();
    @FXML private TextField username;
    @FXML private TextField playername;
    @FXML private Button guardar;
    RadioButton database = DatabaseHelper.getInstance().getSelected();
    @FXML private Text databaseTxt;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        username.setText(user.getUsername().substring(0,1).toUpperCase() + user.getUsername().substring(1));
        playername.setText(user.getPlayername().toUpperCase());
        databaseTxt.setText(database.getText());

        guardar.disableProperty().bind(
                username.textProperty().isEmpty().or(
                playername.textProperty().isEmpty().or(
                playername.textProperty().length().isNotEqualTo(3)
                )));
    }

    @FXML public void saveUpdate() throws IOException {
        File file = new File("./src/main/java/com/anne/retoArcade/Data/users.txt");
        String oldline = user.getUsername()+";"+
                user.getPassword()+";"+
                user.getPlayername()+";"+
                user.getGame1()+";"+user.getGame2()+";"+user.getGame3();

        String newline = username.getText().toLowerCase(Locale.ROOT)+";"+
                user.getPassword()+";"+
                playername.getText().toUpperCase()+";"+
                user.getGame1()+";"+user.getGame2()+";"+user.getGame3();

        List<String> fileContent = new ArrayList<>(Files.readAllLines(file.toPath(), StandardCharsets.UTF_8));
        for (int i = 0; i < fileContent.size(); i++) {
            if (fileContent.get(i).equals(oldline)) {
                fileContent.set(i, newline);
                break;
            }
        }
        Files.write(file.toPath(), fileContent, StandardCharsets.UTF_8);

        user.setUsername(username.getText());
        user.setPlayername(playername.getText());
        change.ToPerfil();
    }

    // ----------------------------------------------------------------------------------

    @FXML public void changeToInicio() throws IOException {
        change.ToInicio();
    }

    @FXML public void changeToGame() throws IOException {
        change.ToGame();
    }

    @FXML public void changeToMusic() throws IOException {
        change.ToMusic();
    }

    @FXML public void changeToJugadores() throws IOException {
        change.ToJugadores();
    }

    @FXML public void changeToPerfil() throws IOException {
        change.ToPerfil();
    }

    @FXML public void goToLogin() throws IOException {
        change.ToLogin();
    }
}