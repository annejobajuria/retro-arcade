package com.anne.retoArcade.controller.actions;

import com.anne.retoArcade.ChangeSceneMethods;
import com.anne.retoArcade.model.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class JugadoresUpController implements Initializable {
    ChangeSceneMethods change = new ChangeSceneMethods();
    Player player = PlayerHelper.getInstance().getPlayer();
    String oldline;
    @FXML private TextField username;
    @FXML private TextField playername;
    @FXML private TextField game1;
    @FXML private TextField game2;
    @FXML private TextField game3;
    @FXML private Button guardar;
    RadioButton database = DatabaseHelper.getInstance().getSelected();
    @FXML private Text databaseTxt;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        databaseTxt.setText(database.getText());

        username.setText(player.getUsername());
        playername.setText(player.getPlayername());
        game1.setText(player.getGame1());
        game2.setText(player.getGame2());
        game3.setText(player.getGame3());
        oldline = player.getUsername()+";"+ player.getPassword()+";"+player.getPlayername()+";"+game1.getText()+";"+game2.getText()+";"+game3.getText();

        guardar.disableProperty().bind(
                username.textProperty().isEmpty().or(
                playername.textProperty().isEmpty().or(
                playername.textProperty().length().isNotEqualTo(3)
                )));

        // player name -> solo 3 caracteres y en mayuscula
        playername.textProperty().addListener((ov, oldValue, newValue) -> {
            playername.setText(newValue.toUpperCase());
            if (newValue.length() > 3) playername.setText(newValue.substring(0, 3));
        });
    }

    @FXML public void saveUpdate() throws IOException {
        String juego1 = game1.getText();
        if (game1.getText().isEmpty()) juego1="-";
        String juego2 = game2.getText();
        if (game2.getText().isEmpty()) juego2="-";
        String juego3 = game3.getText();
        if (game3.getText().isEmpty()) juego3="-";

        File file = new File("./src/main/java/com/anne/retoArcade/Data/users.txt");
        String newline = username.getText()+";"+ player.getPassword()+";"+ playername.getText()+";"+juego1+";"+juego2+";"+juego3;

        List<String> fileContent = new ArrayList<>(Files.readAllLines(file.toPath(), StandardCharsets.UTF_8));
        for (int i = 0; i < fileContent.size(); i++) {
            if (fileContent.get(i).equals(oldline)) {
                fileContent.set(i, newline);
                break;
            }
        }
        Files.write(file.toPath(), fileContent, StandardCharsets.UTF_8);
        changeToJugadores();
    }

    // ----------------------------------------------------------------------------------

    @FXML public void changeToInicio() throws IOException {
        change.ToInicio();
    }

    @FXML public void changeToGame() throws IOException {
        change.ToGame();
    }

    @FXML public void changeToMusic() throws IOException {
        change.ToMusic();
    }

    @FXML public void changeToJugadores() throws IOException {
        change.ToJugadores();
    }

    @FXML public void changeToPerfil() throws IOException {
        change.ToPerfil();
    }

    @FXML public void goToLogin() throws IOException {
        change.ToLogin();
    }
}