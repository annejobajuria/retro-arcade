package com.anne.retoArcade.controller.actions;

import com.anne.retoArcade.ChangeSceneMethods;
import com.anne.retoArcade.model.DatabaseHelper;
import com.anne.retoArcade.model.Juego;
import com.anne.retoArcade.model.JuegoHelper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class JuegosUpdController implements Initializable {
    ChangeSceneMethods change = new ChangeSceneMethods();
    String oldline;
    @FXML private TextField id;
    @FXML private TextField title;
    @FXML private TextField year;
    @FXML private TextArea descrip;
    @FXML private Button guardar;
    RadioButton database = DatabaseHelper.getInstance().getSelected();
    @FXML private Text databaseTxt;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        JuegoHelper helper = JuegoHelper.getInstance();
        Juego juego = helper.getJuego();
        databaseTxt.setText(database.getText());

        id.setText(juego.getId());
        title.setText(juego.getTitle());
        year.setText(juego.getYear());
        descrip.setText(juego.getDesc());
        oldline = id.getText()+";"+title.getText()+";"+year.getText()+";"+descrip.getText();

        guardar.disableProperty().bind(
                id.textProperty().isEmpty().or(
                        title.textProperty().isEmpty().or(
                                year.textProperty().isEmpty()
                        )));

        // id game -> solo numericos
        id.textProperty().addListener((ov, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) id.setText(newValue.replaceAll("[^\\d]", ""));
        });
    }

    @FXML public void saveUpdate() throws IOException {
        String description = descrip.getText();
        if (descrip.getText().isEmpty()) description="-";

        File file = new File("./src/main/java/com/anne/retoArcade/Data/juegos.txt");
        String newline = id.getText()+";"+title.getText()+";"+year.getText()+";"+description;

        List<String> fileContent = new ArrayList<>(Files.readAllLines(file.toPath(), StandardCharsets.UTF_8));
        for (int i = 0; i < fileContent.size(); i++) {
            if (fileContent.get(i).equals(oldline)) {
                fileContent.set(i, newline);
                break;
            }
        }
        Files.write(file.toPath(), fileContent, StandardCharsets.UTF_8);
        changeToGame();
    }

    // ----------------------------------------------------------------------------------

    @FXML public void changeToInicio() throws IOException {
        change.ToInicio();
    }

    @FXML public void changeToGame() throws IOException {
        change.ToGame();
    }

    @FXML public void changeToMusic() throws IOException {
        change.ToMusic();
    }

    @FXML public void changeToJugadores() throws IOException {
        change.ToJugadores();
    }

    @FXML public void changeToPerfil() throws IOException {
        change.ToPerfil();
    }

    @FXML public void goToLogin() throws IOException {
        change.ToLogin();
    }
}