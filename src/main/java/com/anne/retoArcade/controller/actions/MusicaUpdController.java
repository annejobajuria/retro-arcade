package com.anne.retoArcade.controller.actions;

import com.anne.retoArcade.ChangeSceneMethods;
import com.anne.retoArcade.model.DatabaseHelper;
import com.anne.retoArcade.model.Musica;
import com.anne.retoArcade.model.MusicaHelper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class MusicaUpdController implements Initializable {
    ChangeSceneMethods change = new ChangeSceneMethods();
    String oldline;
    @FXML private TextField id;
    @FXML private TextField title;
    @FXML private TextField artist;
    @FXML private TextField disk;
    @FXML private TextField year;
    @FXML private Button guardar;
    RadioButton database = DatabaseHelper.getInstance().getSelected();
    @FXML private Text databaseTxt;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        MusicaHelper helper = MusicaHelper.getInstance();
        Musica musica = helper.getMusica();
        databaseTxt.setText(database.getText());

        id.setText(musica.getId());
        title.setText(musica.getTitle());
        artist.setText(musica.getArtist());
        disk.setText(musica.getDisk());
        year.setText(musica.getYear());
        oldline = id.getText()+";"+title.getText()+";"+artist.getText()+";"+disk.getText()+";"+year.getText();

        guardar.disableProperty().bind(
                id.textProperty().isEmpty().or(
                title.textProperty().isEmpty().or(
                artist.textProperty().isEmpty()
                )));

        // id musica -> solo numericos
        id.textProperty().addListener((ov, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) id.setText(newValue.replaceAll("[^\\d]", ""));
        });
    }

    @FXML public void saveUpdate() throws IOException {
        String disco = disk.getText();
        if (disk.getText().isEmpty()) disco="-";
        String anyo = year.getText();
        if (year.getText().isEmpty()) anyo="-";

        File file = new File("./src/main/java/com/anne/retoArcade/Data/musica.txt");
        String newline = id.getText()+";"+title.getText()+";"+artist.getText()+";"+disco+";"+anyo;

        List<String> fileContent = new ArrayList<>(Files.readAllLines(file.toPath(), StandardCharsets.UTF_8));
        for (int i = 0; i < fileContent.size(); i++) {
            if (fileContent.get(i).equals(oldline)) {
                fileContent.set(i, newline);
                break;
            }
        }
        Files.write(file.toPath(), fileContent, StandardCharsets.UTF_8);
        changeToMusic();
    }

    // ----------------------------------------------------------------------------------

    @FXML public void changeToInicio() throws IOException {
        change.ToInicio();
    }

    @FXML public void changeToGame() throws IOException {
        change.ToGame();
    }

    @FXML public void changeToMusic() throws IOException {
        change.ToMusic();
    }

    @FXML public void changeToJugadores() throws IOException {
        change.ToJugadores();
    }

    @FXML public void changeToPerfil() throws IOException {
        change.ToPerfil();
    }

    @FXML public void goToLogin() throws IOException {
        change.ToLogin();
    }
}