package com.anne.retoArcade.controller.actions;

import com.anne.retoArcade.ChangeSceneMethods;
import com.anne.retoArcade.model.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.RadioButton;
import javafx.scene.text.Text;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class JugadoresDetailController implements Initializable {
    PlayerHelper helper = PlayerHelper.getInstance();
    Player player = helper.getPlayer();
    ChangeSceneMethods change = new ChangeSceneMethods();
    @FXML private Text username;
    @FXML private Text playername;
    @FXML private Text game1;
    @FXML private Text game2;
    @FXML private Text game3;
    @FXML private Text game1name;
    @FXML private Text game2name;
    @FXML private Text game3name;
    RadioButton database = DatabaseHelper.getInstance().getSelected();
    @FXML private Text databaseTxt;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        databaseTxt.setText(database.getText());
        username.setText(player.getUsername().substring(0,1).toUpperCase() + player.getUsername().substring(1));
        playername.setText(player.getPlayername().toUpperCase());

        if (player.getGame1().equals("-")) game1.setText("ø");
        else game1.setText(player.getGame1());
        if (player.getGame2().equals("-")) game2.setText("ø");
        else game2.setText(player.getGame2());
        if (player.getGame3().equals("-")) game3.setText("ø");
        else game3.setText(player.getGame3());

        game1name.setText(searchNameGame(player.getGame1()));
        game2name.setText(searchNameGame(player.getGame2()));
        game3name.setText(searchNameGame(player.getGame3()));
    }

    public String searchNameGame(String id){
        if (id.equals("-")) return "Sin datos";

        List<String> list = new ArrayList<>();
        File file = new File("./src/main/java/com/anne/retoArcade/Data/juegos.txt");
        if (file.exists()) {
            try {
                list = Files.readAllLines(file.toPath(), Charset.defaultCharset());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        for (String line : list) {
            if (!line.equals("")) {
                String[] res = line.split(";");
                if (res[0].equals(id)) return res[1];
            }
        }
        return "No se ha encontrado un juego con esa ID";
    }

    // ----------------------------------------------------------------------------------

    @FXML public void changeToInicio() throws IOException {
        change.ToInicio();
    }

    @FXML public void changeToGame() throws IOException {
        change.ToGame();
    }

    @FXML public void changeToMusic() throws IOException {
        change.ToMusic();
    }

    @FXML public void changeToJugadores() throws IOException {
        change.ToJugadores();
    }

    @FXML public void changeToPerfil() throws IOException {
        change.ToPerfil();
    }

    @FXML public void goToLogin() throws IOException {
        change.ToLogin();
    }
}