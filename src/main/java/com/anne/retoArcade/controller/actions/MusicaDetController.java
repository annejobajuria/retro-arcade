package com.anne.retoArcade.controller.actions;

import com.anne.retoArcade.ChangeSceneMethods;
import com.anne.retoArcade.model.DatabaseHelper;
import com.anne.retoArcade.model.Musica;
import com.anne.retoArcade.model.MusicaHelper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.RadioButton;
import javafx.scene.text.Text;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MusicaDetController implements Initializable {
    ChangeSceneMethods change = new ChangeSceneMethods();
    @FXML private Text title;
    @FXML private Text artist;
    @FXML private Text disk;
    @FXML private Text year;
    RadioButton database = DatabaseHelper.getInstance().getSelected();
    @FXML private Text databaseTxt;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        MusicaHelper helper = MusicaHelper.getInstance();
        Musica musica = helper.getMusica();
        databaseTxt.setText(database.getText());

        title.setText(musica.getTitle());
        artist.setText(musica.getArtist());
        disk.setText(musica.getDisk());
        year.setText(musica.getYear());
    }

    // ----------------------------------------------------------------------------------

    @FXML public void changeToInicio() throws IOException {
        change.ToInicio();
    }

    @FXML public void changeToGame() throws IOException {
        change.ToGame();
    }

    @FXML public void changeToMusic() throws IOException {
        change.ToMusic();
    }

    @FXML public void changeToJugadores() throws IOException {
        change.ToJugadores();
    }

    @FXML public void changeToPerfil() throws IOException {
        change.ToPerfil();
    }

    @FXML public void goToLogin() throws IOException {
        change.ToLogin();
    }
}