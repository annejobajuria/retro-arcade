package com.anne.retoArcade.controller;

import com.anne.retoArcade.ChangeSceneMethods;
import com.anne.retoArcade.model.DatabaseHelper;
import com.anne.retoArcade.model.Player;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class JugadoresController implements Initializable {
    ChangeSceneMethods change = new ChangeSceneMethods();
    @FXML private TableView<Player> table;
    @FXML private TableColumn<Player, String> username;
    @FXML private TableColumn<Player, String> playername;
    @FXML private TableColumn<Player, String> game1;
    @FXML private TableColumn<Player, String> game2;
    @FXML private TableColumn<Player, String> game3;
    RadioButton database = DatabaseHelper.getInstance().getSelected();
    @FXML private Text databaseTxt;

    // ----------------------------------------------------------------------------------

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ObservableList<Player> players = FXCollections.observableArrayList(fillList());
        username.setCellValueFactory(new PropertyValueFactory<>("username"));
        playername.setCellValueFactory(new PropertyValueFactory<>("playername"));
        game1.setCellValueFactory(new PropertyValueFactory<>("game1"));
        game2.setCellValueFactory(new PropertyValueFactory<>("game2"));
        game3.setCellValueFactory(new PropertyValueFactory<>("game3"));
        table.setItems(players);
        databaseTxt.setText(database.getText());
    }

    // ----------------------------------------------------------------------------------

    public List<Player> fillList() {
        List<String> list = new ArrayList<>();
        List<Player> listPlayers = new ArrayList<>();

        File file = new File("./src/main/java/com/anne/retoArcade/Data/users.txt");
        if (file.exists()) {
            try {
                list = Files.readAllLines(file.toPath(), Charset.defaultCharset());
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (list.isEmpty())
                return null;
        }
        for (String line : list) {
            if (!line.equals("")) {
                String[] res = line.split(";");
                listPlayers.add(new Player(res[0], res[1], res[2], res[3], res[4], res[5]));
            }
        }
        return listPlayers;
    }

    @FXML public void viewDetails() throws IOException {
        Player player = table.getSelectionModel().getSelectedItem();
        change.ToDetailJugador(player);
    }

    @FXML public void update() throws IOException {
        Player player = table.getSelectionModel().getSelectedItem();
        change.ToUpdateJugador(player);
    }

    @FXML public void delete() throws IOException {
        Player player = table.getSelectionModel().getSelectedItem();
        if (player==null) return;
        String oldline = player.getUsername()+";"+player.getPassword()+";"+player.getPlayername()+";"+player.getGame1()+";"+player.getGame2()+";"+player.getGame3();
        File file = new File("./src/main/java/com/anne/retoArcade/Data/users.txt");
        List<String> out = Files.lines(file.toPath())
                .filter(line -> !line.contains(oldline))
                .collect(Collectors.toList());
        Files.write(file.toPath(), out, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
        change.ToJugadores();
    }

    // ----------------------------------------------------------------------------------

    @FXML public void changeToInicio() throws IOException {
        change.ToInicio();
    }

    @FXML public void changeToGame() throws IOException {
        change.ToGame();
    }

    @FXML public void changeToMusic() throws IOException {
        change.ToMusic();
    }

    @FXML public void changeToJugadores() throws IOException {
        change.ToJugadores();
    }

    @FXML public void changeToPerfil() throws IOException {
        change.ToPerfil();
    }

    @FXML public void goToLogin() throws IOException {
        change.ToLogin();
    }
}