package com.anne.retoArcade.controller;

import com.anne.retoArcade.ChangeSceneMethods;
import com.anne.retoArcade.model.User;
import com.anne.retoArcade.model.UserHelper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ResourceBundle;

public class SignupController implements Initializable {
    ChangeSceneMethods change = new ChangeSceneMethods();
    @FXML private Button signupBtn;
    @FXML private TextField usernameSignup;
    @FXML private TextField playernameSignup;
    @FXML private PasswordField passSignup;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // player name -> solo 3 caracteres y en mayuscula
        playernameSignup.textProperty().addListener((ov, oldValue, newValue) -> {
            playernameSignup.setText(newValue.toUpperCase());
            if (newValue.length() > 3) playernameSignup.setText(newValue.substring(0, 3));
        });

        signupBtn.disableProperty().bind(
                usernameSignup.textProperty().isEmpty().or(
                playernameSignup.textProperty().isEmpty().or(
                playernameSignup.textProperty().length().isNotEqualTo(3).or(
                passSignup.textProperty().isEmpty()
                ))));
    }

    @FXML public void verify() throws IOException {
        File file = new File("./src/main/java/com/anne/retoArcade/Data/users.txt");
        String newline = "\n"+usernameSignup.getText() +";"+playernameSignup.getText().toUpperCase() +";"+passSignup.getText();
        Files.write(file.toPath(), newline.getBytes(), StandardOpenOption.APPEND);
        User user = new User(usernameSignup.getText(), passSignup.getText().toUpperCase(), playernameSignup.getText(), "-", "-", "-");
        UserHelper holder = UserHelper.getInstance();
        holder.setUser(user);
        change.ToInicio();
    }

    // ----------------------------------------------------------------------------------

    @FXML public void changeToLogin() throws IOException {
        change.ToLogin();
    }
}
