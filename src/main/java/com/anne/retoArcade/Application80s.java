package com.anne.retoArcade;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Application80s extends javafx.application.Application {
    public static Stage stage_app;

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) throws IOException {
        stage_app = stage;

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("login.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        stage_app.setTitle("Recreativos 80's");
        stage_app.setScene(scene);
        stage_app.show();
    }
}